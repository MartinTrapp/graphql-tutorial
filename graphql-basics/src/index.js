import { GraphQLServer } from 'graphql-yoga'

//type definitions (schema)
const typeDefs = `
    type Query {
        grades: [Int!]!
        add(a: Float!, b: Float): Float!
        addAll(elements: [Int!]!): Int!
        greeting(name: String): String! 
        me: User!
        post: Post!
    }

    type User {
        id: ID!
        name: String!
        email: String!
        age: Int
    }

    type Post {
        id: ID!
        published: Boolean!
        title: String!
        body: String!
    }
`;

//resolvers
const resolvers = {
    Query: {
        grades(){
            return [1,2,3];
        },
        add(parent, args, info, ctx) {
          return args.a + args.b;  
        },
        addAll(parent, args, info, ctx){
            if(args.elements.length === 0){
                return 0;
            }
            
            return args.elements.reduce((accumulator, currentvalue) => {
                return accumulator + currentvalue;
            });

            return 42;
        },    
        greeting(parent, args, info, ctx) {
            if(args.name){
                return `hello ${args.name}!`
            } else {
                return 'hello'
            } 
        }, 
        me() {
            return {
                id: '123',
                name: 'DrFoo',
                email: 'drfoo@bar.com',
                age: 42
            }
        },
        post() {
            return {
                id: '567',
                published: true ,
                title: 'first post ever',
                body: 'the trues is the news'
            }
        }
    }
}

const server = new GraphQLServer({
    typeDefs,
    resolvers
})

server.start(() => {
    console.log('server is running');
})